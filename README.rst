=====================
Backgrounds slideshow
=====================

It is a **Python 3** script in order to create dynamic cyclic desktop
background. It supports `MATE`_, `GNOME`_, `Cinnamon`_ and `Xfce`_ (experimental).

For the Xfce desktop environment, **-R** **-E**, and **-e** options are not working.
With this desktop, we use the org.xfce.Xfconf service.

This program is useful for desktops which depend of GSettings features.

To get a copy:

::

   git clone https://bitbucket.org/olivierduchateau/backgrounds-slideshow.git

Overview
========

::

   ./backgrounds-slideshow.py -h
   usage: background-slideshow.py [-h] [-E REGEX] [-e FILE] [-R] [-r] [-t MIN] -d DIRECTORY
   
   optional arguments:
      -h, --help            show this help message and exit
      -E REGEX, --extended-regexp REGEX
                            use regular expression (can be specified multiple
                            times)
      -e FILE, --exclude FILE
                            image to exclude (can be specified multiple times)
      -R, --recursive       scan directory recursively
      -r, --random          shuffle elements
      -t MIN, --time MIN    how long image is going to be displayed (default
                            10min)
      -d DIRECTORY, --directory DIRECTORY
                            directory to scan

Example
=======

If you want to exclude some files:

::

   ./backgrounds-slideshow.py -e adwaita-morning.jpg -e adwaita-night.jpg -e endless-shapes.jpeg -d /usr/share/backgrounds/gnome/

Or use regular expression:

::

   ./backgrounds-slideshow.py -r -E adwaita-*.jpg -d /usr/share/backgrounds/gnome/

You can combine both:

::

   ./backgrounds-slideshow.py -r -d /usr/share/backgrounds/gnome/ -E adwaita-*.jpg -e symbolics-2.jpg

Dependencies
============

Debian and Ubuntu-like
~~~~~~~~~~~~~~~~~~~~~~

* python3
* python3-pil
* libgtk-3-dev
* python3-gi
* gir1.2-glib-2.0
* gir1.2-gtk-3.0

* xfconf (only for Xfce users)

Fedora
~~~~~~

* python3
* python3-pillow
* python3-gobject
* glib2-devel
* gtk3-devel

* xfconf (only for Xfce users)

FreeBSD
~~~~~~~

* python3
* py3X-pillow
* p3X-gobject3
* glib
* gtk3

* xfce4-conf (only for Xfce users)

Where *X* is 6 or higher.

.. _MATE: https://mate-desktop.org/
.. _GNOME: https://www.gnome.org/
.. _Cinnamon: https://github.com/linuxmint/Cinnamon
.. _Xfce: https://xfce.org/
