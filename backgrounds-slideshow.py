#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright (c) 2018, 2019, Olivier Duchateau <duchateau.olivier@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

'''From a directory allow to make a slideshow for the
GNOME, Mate, Cinnamon, and Xfce (it is still experimental) desktops.
'''

import argparse
import datetime
import fnmatch
import imghdr
import os
import pathlib
import random
import re
import sys
import uuid
import xml.etree.ElementTree as ET

try:
    import PIL.Image
    import gi

    gi.require_version('GLib', '2.0')
    gi.require_version('Gio', '2.0')
    gi.require_version('Gdk', '3.0')

    from gi.repository import GLib, Gio, Gdk
except (ImportError, ValueError):
    print('Additional modules are required')
    sys.exit(-1)


class Backgrounds(object):
    '''Get list of images (pathlib.Path object) used as background.'''

    def __init__(self, dirname, recursive, exclude):
        self._files = []
        # pathlib.Path object
        if isinstance(dirname, pathlib.Path):
            dir_name = dirname
        else:
            dir_name = pathlib.Path(dirname)

        if exclude is not None:
            self.pattern = self.build_regex(exclude)
        else:
            self.pattern = None

        if recursive:
            self.get_files_recursive(dir_name)
        else:
            self.get_files(dir_name)

    def build_regex(self, seq):
        '''Return re.compile object.'''
        s = '|'.join([fnmatch.translate(i) for i in seq])

        return re.compile(s)

    def valid_file(self, filename):
        res = False
        formats = ['png', 'jpeg', 'webp']

        if filename.is_file() and not filename.is_symlink():
            # Get basename
            n = filename.name

            if self.pattern is not None:
                if not self.pattern.match(n):
                    # Cast pathlib.Path object to string
                    value = imghdr.what('{0}'.format(filename))
                    if value in formats:
                        res = True
            else:
                # Cast pathlib.Path object to string
                value = imghdr.what('{0}'.format(filename))
                if value in formats:
                    res = True

        return res

    def get_files(self, root):
        '''Get list of valid images.'''

        with os.scandir(root) as files:
            for i in files:
                filename = pathlib.Path(i.path)
                if self.valid_file(filename):
                    self._files.append(filename)

    def get_files_recursive(self, root):
        '''Get list of valid images.'''

        for dirpath, dirs, files in os.walk(root):
            for i in files:
                filename = pathlib.Path(dirpath) / i
                if self.valid_file(filename):
                    self._files.append(filename)

    @property
    def list_img(self):
        '''Return list of pathlib.Path object.'''
        # We need 2 or more images
        if len(self._files) >= 2:
            #self._files.sort()

            # Case-insensitive string comparaison
            return sorted(self._files,
                          key=lambda str: '{0}'.format(str).lower())
        else:
            return []


class XfconfBackend(object):
    '''For desktop environment using Xfconf backend.'''

    def __init__(self, files, timer, shuffle):
        self.img = files
        self.timer = timer
        self.shuffle = False
        if shuffle:
            random.shuffle(self.img)
            self.shuffle = True

        self.channel = 'xfce4-desktop'

        session = Gio.bus_get_sync(Gio.BusType.SESSION, None)
        self.proxy = Gio.DBusProxy.new_sync(session,
                                            Gio.DBusProxyFlags.NONE,
                                            None,
                                            'org.xfce.Xfconf',
                                            '/org/xfce/Xfconf',
                                            'org.xfce.Xfconf',
                                            None)

    def get_monitor_name(self):
        '''Return a string identifying the monitor model.'''
        res = None

        display = Gdk.Display.get_default()
        if display is not None:
            # Where "main desktop" lives
            monitor = display.get_primary_monitor()
            if monitor is not None:
                res = monitor.get_model()

        return res

    # Not yet used, maybe in future...
    def get_workspace_count(self):
        n_workspace = self.proxy.GetProperty('(ss)', 'xfwm4',
                                             '/general/workspace_count')

        return n_workspace

    def get_parents(self, path):
        tokens = path.split('/')

        return '/'.join(tokens[:-2])

    def lookup_backdrop_property(self):
        '''Find /backdrop/screen... prefix.'''
        res = None

        props = self.proxy.GetAllProperties('(ss)', self.channel, '/')
        if props:
            for i in props.keys():
                if '/backdrop/screen' in i:
                    res = self.get_parents(i)
                    if res is not None:
                        break

        return res

    def turn_on_backdrop_cycle(self, prefix, prop_name):
        workspace_id = 0
        prop = '{0}/workspace{1}/{2}'.format(prefix, workspace_id,
                                             prop_name)
        if prop.startswith('/'):
            self.proxy.SetProperty('(ssv)', self.channel, prop,
                                   GLib.Variant.new_boolean(True))

    def turn_on_random_order(self, prefix, prop_name):
        workspace_id = 0
        prop = '{0}/workspace{1}/{2}'.format(prefix, workspace_id,
                                             prop_name)
        if prop.startswith('/'):
            self.proxy.SetProperty('(ssv)', self.channel, prop,
                                   GLib.Variant.new_boolean(True))

    def set_backdrop_last_image(self, prefix, prop_name, first_img):
        workspace_id = 0
        prop = '{0}/workspace{1}/{2}'.format(prefix, workspace_id,
                                             prop_name)
        if prop.startswith('/'):
            self.proxy.SetProperty('(ssv)', self.channel, prop,
                                   GLib.Variant.new_string(first_img))

    def set_backdrop_cycle_timer(self, prefix, prop_name):
        workspace_id = 0
        prop = '{0}/workspace{1}/{2}'.format(prefix, workspace_id,
                                             prop_name)
        if prop.startswith('/'):
            self.proxy.SetProperty('(ssv)', self.channel, prop,
                                   GLib.Variant.new_uint32(self.timer))

    def change_background_properties(self, screen_id=0):
        prefix = None
        monitor_name = self.get_monitor_name()
        #n_workspace = self.get_workspace_count()

        # We build property
        if monitor_name is not None:
            # Currently, we support only one screen
            prefix = '/backdrop/screen{0}/monitor{1}'.format(screen_id,
                                                             monitor_name)
        else:
            prefix = self.lookup_backdrop_property()

        if prefix is not None:
            # Set properties
            self.turn_on_backdrop_cycle(prefix, 'backdrop-cycle-enable')
            self.set_backdrop_cycle_timer(prefix, 'backdrop-cycle-timer')
            if self.shuffle:
                self.turn_on_random_order(prefix,
                                          'backdrop-cycle-random-order')
            self.set_backdrop_last_image(prefix, 'last-image',
                                         '{0}'.format(self.img[0]))
        else:
            print('Error: property not found')
            sys.exit(0)


class GSettingsBackend(object):
    '''For desktop environment using GSettings backend.'''

    def __init__(self, files, duration, shuffle):
        self.img = files
        self.duration = 60 * duration
        if shuffle:
            random.shuffle(self.img)

        now = datetime.datetime.today()
        self.cal = {'year': '{0:04d}'.format(now.year),
                    'month': '{0:02d}'.format(now.month),
                    'day': '{0:02d}'.format(now.day),
                    'hour': '{0:02d}'.format(now.hour),
                    'minute': '{0:02d}'.format(now.minute),
                    'second': '{0:02d}'.format(now.second)}

        p = pathlib.Path(GLib.get_user_cache_dir())
        filename = p / '{0}-slideshow'.format(uuid.uuid4())
        # We get an pathlib.Path object
        self.output = filename.with_suffix('.xml')

    def set_size_img(self, parent, tag, img_name):
        try:
            with PIL.Image.open(img_name) as img:
                width, height = img.size

                node = self.set_subnode(parent, tag)
                self.set_text(self.set_subnode(node, 'size',
                                               attrib={'width': '{0}'.format(width),
                                                       'height': '{0}'.format(height)}),
                              '{0}'.format(img_name))
        except IOError:
            self.set_text(self.set_subnode(parent, tag),
                          '{0}'.format(img_name))

    def set_node(self, tag, attrib={}):
        return ET.Element(tag, attrib)

    def set_subnode(self, parent, tag, attrib={}):
        return ET.SubElement(parent, tag, attrib=attrib)

    def set_text(self, node, content):
        if isinstance(node, ET.Element):
            node.text = content
        elif isinstance(node, ET.SubElement):
            node.text = content

    def generate_xml(self):
        # Root element
        root = self.set_node('background')

        # Date and time block
        dt = self.set_node('starttime')
        for i in iter(self.cal.keys()):
            child = self.set_subnode(dt, i)
            self.set_text(child, self.cal.get(i))
        root.append(dt)

        # Files
        length = len(self.img)
        i = 0
        while i < length:
            stat = self.set_node('static')
            self.set_text(self.set_subnode(stat, 'duration'),
                          '{0:.1f}'.format(self.duration))
            self.set_size_img(stat, 'file', self.img[i])
            root.append(stat)

            trans = self.set_node('transition',
                                  attrib={'type': 'overlay'})
            self.set_text(self.set_subnode(trans, 'duration'),
                          '{0:.1f}'.format(5))
            self.set_size_img(trans, 'from', self.img[i])
            if (i + 1) < length:
                name = self.img[(i + 1)]
            elif (i + 1) >= length:
                name = self.img[0]
            self.set_size_img(trans, 'to', name)
            root.append(trans)
            name = None

            i += 1

        tree = ET.ElementTree(root)
        tree.write(self.output, encoding='utf-8', xml_declaration=True)

    def set_settings_from_key(self, schema_id, key, transform):
        res = False

        # If an URI is expected
        if transform:
            g_file = Gio.File.new_for_path('{0}'.format(self.output))
            value = g_file.get_uri()
        else:
            value = '{0}'.format(self.output)

        # Get the default system schema source
        src = Gio.SettingsSchemaSource.get_default()
        if src is not None:
            # We ensure schema with identifier exists
            schema = src.lookup(schema_id, False)
            if schema is not None and schema.has_key(key):
                settings = Gio.Settings.new(schema_id)
                res = settings.set_value(key, GLib.Variant('s', value))

        return res

    def remove_old_files(self):
        '''Delete previous pathlib.Path objects.'''
        if isinstance(self.output, pathlib.Path):
            for i in iter(self.output.parent.glob('*-slideshow.xml')):
                if i != self.output:
                    i.unlink()

    @property
    def xml_file(self):
        '''Return pathlib.Path object.'''
        return self.output


def absolute_path(path):
    if path.startswith('~'):
        p = pathlib.Path(path).expanduser()
    else:
        p = pathlib.Path(path).resolve()

    return p

def check_directory(path):
    res = False

    if isinstance(path, pathlib.Path):
        if path.is_dir() and path.exists():
            res = True

    return res

def main(args):
    root_dir = args.directory

    if check_directory(root_dir):
        # Which is desktop environment?
        desktop = GLib.environ_getenv(GLib.get_environ(),
                                      'XDG_CURRENT_DESKTOP').lower()
        if desktop == 'xfce':
            bg = Backgrounds(root_dir, False, None)
            if bg.list_img:
                backend = XfconfBackend(bg.list_img, args.time,
                                        args.random)
                backend.change_background_properties()
            else:
                print('Error: not enough files')
                sys.exit(-1)
        else:
            # Files to ignore
            if args.exclude is not None and args.regex is not None:
                args.exclude.extend(args.regex)
                ignore_files = args.exclude
            elif args.exclude is not None and args.regex is None:
                ignore_files = args.exclude
            elif args.exclude is None and args.regex is not None:
                ignore_files = args.regex
            elif args.exclude is None and args.regex is None:
                ignore_files = None

            bg = Backgrounds(root_dir, args.recursive, ignore_files)
            if bg.list_img:
                if desktop == 'mate':
                    schema_id = 'org.mate.background'
                    key = 'picture-filename'
                    transform = False
                elif desktop == 'gnome' or desktop == 'cinnamon':
                    schema_id = 'org.{0}.desktop.background'.format(desktop)
                    key = 'picture-uri'
                    transform = True
                else:
                    raise ValueError('Desktop environment not supported')
                    sys.exit(0)

                backend = GSettingsBackend(bg.list_img, args.time,
                                           args.random)
                backend.generate_xml()
                if backend.xml_file.exists():
                    res = backend.set_settings_from_key(schema_id, key,
                                                        transform)
                    if res:
                        backend.remove_old_files()
                    else:
                        backend.xml_file.unlink()
            else:
                print('Error: not enough files')
                sys.exit(-1)
    else:
        print('Error: no such file or directory')
        sys.exit(-1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-E', '--extended-regexp', dest='regex',
                        action='append',
                        help='use regular expression '
                             '(can be specified multiple times)')
    parser.add_argument('-e', '--exclude', action='append',
                        metavar='FILE',
                        help='image to exclude '
                             '(can be specified multiple times)')
    parser.add_argument('-R', '--recursive', action='store_true',
                        help='scan directory recursively')
    parser.add_argument('-r', '--random', action='store_true',
                        help='shuffle elements')
    parser.add_argument('-t', '--time', type=int, default=10,
                        metavar='MIN',
                        help='how long image is going to be displayed '
                             '(default 10min)')
    parser.add_argument('-d', '--directory', required=True,
                        type=absolute_path,
                        help='directory to scan')
    main(parser.parse_args())
